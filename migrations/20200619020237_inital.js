exports.up = function (knex) {
  return knex.schema
    .createTable("messages", function (table) {
      table.bigInteger("id").unsigned().index();
      table.text("content").notNullable();
    })
    .createTable("commands", function (table) {
      table.increments("id");
      table.string("name", 255).notNullable().index();
      table.text("content").notNullable();

      table.unique("name");
    });
};

exports.down = function (knex) {
  return knex.schema.dropTable("messages");
};
