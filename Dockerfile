FROM node:14-alpine

ENV BOT_TOKEN="" \
    OWNER_ID="" \
    PREFIX="~" \
    DB_URL=""

RUN mkdir /app

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn

COPY . .

CMD [ "node", "index.js" ]
