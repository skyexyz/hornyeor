const MarkovChain = require('purpl-markov-chain');
const Discord = require("discord.js");
const randomFloat = require("random-floating");
const Knex = require("knex");
const { uniqBy } = require("lodash/array");

const client = new Discord.Client();

const { BOT_TOKEN, OWNER_ID, PREFIX, DB_URL } = process.env;

const knex = Knex({
  client: "postgresql",
  connection: DB_URL,
});

let messageCache = [];
let markovDb = [];
let markov = [];
let ready = false;

const markovOptions = {
  maxTries: 1000,
  prng: Math.random,
  filter: (result) => result.string.split(" ").length >= 1,
};

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
  knex.migrate
    .latest({
      client: "postgresql",
      connection: DB_URL,
    })
    .then(() => {
      regenerateMarkov();
    });
});

client.on("message", (message) => {
  if (message.author.id === client.user.id || !ready) {
    return;
  }

  if (message.mentions.users.get(client.user.id)) {
    try {
      const result = markov.generate();
      message.reply(result);
    } catch (ex) {
      console.error("unable to generate message");
    }
  } else if (message.content.startsWith(PREFIX)) {
    const command = message.content.split(" ")[0].substring(1);
    switch (command) {
      case "train":
        if (message.author.id === OWNER_ID) {
          train(message);
        }
        break;
      case "talk":
        talk(message);
        break;
      case "learn":
        const learnArgs = message.content.substring(command.length + 2);
        learn(
          message,
          learnArgs.split(" ")[0],
          learnArgs.substring(learnArgs.split(" ")[0].length + 1)
        );
        break;
      case "gimme":
        const gimmeArgs = message.content.substring(command.length + 2);
        gimme(message, gimmeArgs.split(" ")[0]);
        break;
      case "forget":
        const forgetArgs = message.content.substring(command.length + 2);
        forget(message, forgetArgs.split(" ")[0]);
        break;
      case "poem":
        const poemArgs = message.content.substring(command.length + 2);
        poem(message, poemArgs.split(" "));
        break;
    }
  } else if (message.content.toLowerCase().indexOf("hornyeor") !== -1) {
    talk(message);
  } else {
    messageCache.push({
      id: message.id,
      content: message.content,
    });

    if (randomFloat({ min: 0, max: 100, fixed: 2 }) <= 4.2) {
      talk(message);
    }
    if (messageCache.length >= 2000) {
      ready = false;
      message.channel.send("I'm boofin up!");
      regenerateMarkov();
      ready = true;
      message.channel.send("Startup complete");
    }
  }
});

client.login(BOT_TOKEN);

const talk = (message) => {
  try {
    const result = markov.generate();
    message.channel.send(result);
  } catch {
    console.error("unable to generate message");
  }
};

const PAGE_SIZE = 100;
/**
 * @param {Discord.Message} message
 */
const train = async (message) => {
  console.log("training");
  let historyCache = [];
  let keepGoing = true;
  let lastMessageID;

  while (keepGoing) {
    const messages = await message.channel.messages.fetch({
      before: lastMessageID,
      limit: PAGE_SIZE,
    });

    const nonBotMessageFormatted = messages
      .filter((msg) => !msg.author.bot)
      .map((msg) => {
        const msgObj = {
          string: msg.content,
          id: msg.id,
        };
        if (msg.attachments.size > 0) {
          msgObj.attachment = msg.attachments.values().next().value.url;
        }

        return msgObj;
      });

    historyCache = historyCache.concat(nonBotMessageFormatted);
    lastMessageID = messages.last().id;
    if (messages.size < PAGE_SIZE) {
      keepGoing = false;
    }
  }

  messageCache = messageCache.concat(historyCache);
  await regenerateMarkov();
  message.reply(`Finished training from past ${historyCache.length} messages.`);
};

const regenerateMarkov = async () => {
  console.log("Regenerating Markov corpus...");
  let messages = messageCache.slice();
  messageCache = [];
  try {
    markovDb = await knex("messages").select("*");
    markovDb = markovDb.map((x) => {
      return {
        id: x.id,
        string: x.content,
      };
    });
  } catch (err) {
    markovDb = {
      messages: [
        {
          id: "0",
          string: "",
        },
      ],
    };
  }

  if (markovDb.length === 0) {
    markovDb = [
      {
        id: "0",
        string: "",
      },
    ];
  }

  if (messages.length > 0) {
    await knex("messages").insert(
      messages
        .filter((x) => !markovDb.find((m) => m.id === x.id))
        .map((x) => {
          return {
            id: x.id,
            content: x.string,
          };
        })
    );
  }
  markovDb = uniqBy(markovDb.concat(messages), "id");

  markov = new MarkovChain();
  for (let i = 0; i < markovDb.length; i++) {
    markov.update(markovDb[i].string);
  }
  ready = true;
  console.log("Done regenerating corpus");
};

const learn = async (message, name, content) => {
  const command = await knex("commands").where("name", name);
  if (command.length === 0) {
    await knex("commands").insert({ name, content });
  } else {
    await knex("commands").update({ content }).where("id", command[0].id);
  }

  message.reply(`I've learned that about ${name}`);
};

const gimme = async (message, name) => {
  const command = await knex("commands").where("name", name);
  if (command.length > 0) {
    message.channel.send(command[0].content);
  }
};

const forget = async (message, name) => {
  try {
    const command = await knex("commands").delete().where("name", name);
  } catch {}
  message.reply("forgotten!");
};

const markovPoemOptions = (word) => {
  return {
    maxTries: 1000,
    prng: Math.random,
    filter: (result) =>
      result.string.indexOf(word) === 0 && result.string.split(" ").length >= 1,
  };
};

const poem = async (message, words) => {
  let poem = "___" + words.join(" ") + "___\n";
  let length = words.length;
  for (let i = 0; i < length; i++) {
    let result = "";
    try {
      result = markov.generate({ from: words[i] });
      if (result === '') {
        result = markov.generate();
      }
    } catch {
      try {
        result = markov.generate();
      } catch {
        console.error("unable to generate message");
      }
    }
    if (result) {
      poem += result + "\n";
    }
  }
  if (length < 3) {
    for (let i = length; i < 4; i++) {
      poem += markov.generate() + "\n";
    }
  }
  poem += "FIN"
  message.channel.send(poem);
};
