// Update with your config settings.

module.exports = {
  development: {
    client: "postgresql",
    connection: "postgres://hornyeor:hornyeor@postgres/hornyeor",
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },

  staging: {
    client: "postgresql",
    connection: "postgres://hornyeor:hornyeor@postgres/hornyeor",
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },

  production: {
    client: "postgresql",
    connection: "postgres://hornyeor:hornyeor@postgres/hornyeor",
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
};
